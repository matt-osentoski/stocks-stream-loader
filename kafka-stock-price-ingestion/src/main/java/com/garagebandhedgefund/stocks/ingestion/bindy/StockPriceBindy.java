package com.garagebandhedgefund.stocks.ingestion.bindy;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import java.util.Date;

/**
 * This bindy is used to Map CSV files from https://www.alphavantage.co
 * TIME_SERIES_DAILY
 */

@CsvRecord( separator = ",", skipFirstLine = true )
public class StockPriceBindy {

    @DataField(pos = 1, pattern = "yyyy-MM-dd")
    private Date timestamp;

    @DataField(pos = 2, defaultValue = "0")
    private double open;

    @DataField(pos = 3, defaultValue = "0")
    private double high;

    @DataField(pos = 4, defaultValue = "0")
    private double low;

    @DataField(pos = 5, defaultValue = "0")
    private double close;

    @DataField(pos = 6, defaultValue = "0")
    private long volume;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }
}
