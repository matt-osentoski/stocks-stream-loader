package com.garagebandhedgefund.stocks.stream;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Properties;

/**
 * This class filters a stock-upsert topic by the market cap. size.
 *
 * This class was derived from an example on github:
 * https://github.com/jkorab/kafka-streams-example
 */
@Component
public class KafkaStockCombineStream {

    @Value("${kafka.brokers.uri}")
    private String brokerUris;

    @Value("${kafka.streams.combine.application.id}")
    private String applicationId;

    @Value("${kafka.stocks.custom.topic}")
    private String stocksCustomTopic;

    @Value("${kafka.stocks.largecap.topic}")
    private String largecapTopic;

    @Value("${kafka.available.stocks.upsert.topic}")
    private String availableStocksUpsertTopic;

    private KafkaStreams streams;

    @PostConstruct
    public void runStream() {
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, brokerUris);

        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        KStreamBuilder builder = new KStreamBuilder();
        builder.stream(largecapTopic, stocksCustomTopic)
                .to(availableStocksUpsertTopic);

        streams = new KafkaStreams(builder, config);
        streams.cleanUp();  // Reset the application against the Kafka cluster before beginning.
        streams.start();
    }

    @PreDestroy
    public void closeStream() {
        streams.close();
    }
}
