package com.garagebandhedgefund.stocks.batchjob.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class StringUtilsTest {

    private final static String MARKET_CAP = "$887.95B";
    private final static long MARKET_CAP_LONG = 887950000000L;

    @Test
    public void marketCapToLong() {
        assertEquals(MARKET_CAP_LONG, StringUtils.marketCapToLong(MARKET_CAP));
        assertNotEquals(-1, StringUtils.marketCapToLong(MARKET_CAP));
        assertEquals(71510000,StringUtils.marketCapToLong("$71.51M"));
        assertNotEquals(-1, StringUtils.marketCapToLong("$71.51M"));
        assertEquals(0, StringUtils.marketCapToLong("n/a"));
    }
}