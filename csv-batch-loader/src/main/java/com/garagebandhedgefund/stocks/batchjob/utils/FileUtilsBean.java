package com.garagebandhedgefund.stocks.batchjob.utils;

import org.apache.camel.util.FileUtil;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileUtilsBean {

    public void deleteFile(String absoluteFilePath) {
        if (absoluteFilePath != null && !("").equals(absoluteFilePath.trim())) {
            FileUtil.deleteFile(new File(absoluteFilePath));
        }
    }
}
