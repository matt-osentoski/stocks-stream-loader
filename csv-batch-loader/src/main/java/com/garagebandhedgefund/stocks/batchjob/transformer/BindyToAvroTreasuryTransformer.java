package com.garagebandhedgefund.stocks.batchjob.transformer;

import com.garagebandhedgefund.avro.model.Treasury;
import com.garagebandhedgefund.stocks.batchjob.bindy.TreasuryBindy;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class BindyToAvroTreasuryTransformer {
    final static Logger logger = LoggerFactory.getLogger(BindyToAvroTreasuryTransformer.class);

    public Treasury transform(TreasuryBindy treasuryBindy) {
        Treasury treasury = new Treasury();
        try {
            BeanUtils.copyProperties(treasury, treasuryBindy);
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(), e);
        }
        return treasury;
    }
}
