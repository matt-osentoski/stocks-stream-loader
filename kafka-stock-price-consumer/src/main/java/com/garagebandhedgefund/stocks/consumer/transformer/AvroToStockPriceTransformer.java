package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.avro.model.StockPrice;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class AvroToStockPriceTransformer {
    final static Logger logger = LoggerFactory.getLogger(AvroToStockPriceTransformer.class);

    public com.garagebandhedgefund.trading.model.StockPrice transform(StockPrice avroPrice) {
        com.garagebandhedgefund.trading.model.StockPrice stockPrice =
                new com.garagebandhedgefund.trading.model.StockPrice();
        try {
            BeanUtils.copyProperties(stockPrice, avroPrice);
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(), e);
        }
        return stockPrice;
    }
}
