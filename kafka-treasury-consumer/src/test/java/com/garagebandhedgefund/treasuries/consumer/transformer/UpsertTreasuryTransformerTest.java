package com.garagebandhedgefund.treasuries.consumer.transformer;

import com.garagebandhedgefund.trading.model.Treasury;
import com.garagebandhedgefund.treasuries.consumer.repository.TreasuryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class UpsertTreasuryTransformerTest {

    @Mock
    private TreasuryRepository treasuryRepository;

    @InjectMocks
    private UpsertTreasuryTransformer upsertTreasuryTransformer;

    private Treasury treasury;
    private Date treasuryDate;

    @Before
    public void setUp() throws Exception {
        treasuryDate = new Date();
        treasury = new Treasury();
        treasury.setC1year(1.0);
        treasury.setC2year(2.0);
        treasury.setC10year(3.0);
        treasury.setC30year(4.0);
        treasury.setTimePeriod(treasuryDate);
    }

    @Test
    public void transform() {
        when(treasuryRepository.findByTimePeriod(treasuryDate)).thenReturn(treasury);
        Treasury t = upsertTreasuryTransformer.transform(treasury);
        assertNotNull(t);
        assertEquals(1.0, t.getC1year(),1);
        assertEquals(2.0, t.getC2year(),1);
        assertEquals(3.0, t.getC10year(),1);
        assertEquals(4.0, t.getC30year(),1);
        assertEquals(treasuryDate.getTime(), t.getTimePeriod().getTime());
    }
}