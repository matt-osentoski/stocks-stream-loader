package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.avro.model.StockPrice;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class AvroToStockPriceTransformerTest {

    private StockPrice avroStockPrice;
    private Date now;

    @Before
    public void setup() throws Exception {
        now = new Date();
        avroStockPrice = new StockPrice();
        avroStockPrice.setSymbol("AAPL");
        avroStockPrice.setPeriod("d");
        avroStockPrice.setOpeningPrice(3.10);
        avroStockPrice.setLowPrice(3.00);
        avroStockPrice.setHighPrice(4.20);
        avroStockPrice.setClosingPrice(3.14);
        avroStockPrice.setPriceDate(now.getTime());
    }

    @Test
    public void transform() {
        AvroToStockPriceTransformer transformer = new AvroToStockPriceTransformer();
        com.garagebandhedgefund.trading.model.StockPrice sp = transformer.transform(avroStockPrice);
        assertNotNull(sp);
        assertEquals("AAPL", sp.getSymbol());
        assertEquals("d", sp.getPeriod());
        assertEquals(3.10, sp.getOpeningPrice(), 0);
        assertEquals(3.00, sp.getLowPrice(), 0);
        assertEquals(4.20, sp.getHighPrice(), 0);
        assertEquals(3.14, sp.getClosingPrice(), 0);
        assertEquals(now.getTime(), sp.getPriceDate().getTime());

        // Negative tests
        assertNotEquals(1.10, sp.getOpeningPrice(), 0);
        assertNotEquals(-8.00, sp.getLowPrice(), 0);
        assertNotEquals(0.20, sp.getHighPrice(), 0);
        assertNotEquals(13.14, sp.getClosingPrice(), 0);
    }
}