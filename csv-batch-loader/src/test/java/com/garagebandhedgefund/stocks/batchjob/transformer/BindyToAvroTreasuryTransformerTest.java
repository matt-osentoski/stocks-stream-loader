package com.garagebandhedgefund.stocks.batchjob.transformer;

import com.garagebandhedgefund.avro.model.Treasury;
import com.garagebandhedgefund.stocks.batchjob.bindy.TreasuryBindy;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BindyToAvroTreasuryTransformerTest {

    private TreasuryBindy treasuryBindy;
    private Date treasuryDate;

    @Before
    public void setUp() throws Exception {
        treasuryDate = new Date();
        treasuryBindy = new TreasuryBindy();
        treasuryBindy.setC1year(1.0);
        treasuryBindy.setC2year(2.0);
        treasuryBindy.setC10year(3.0);
        treasuryBindy.setC30year(4.0);
        treasuryBindy.setTimePeriod(treasuryDate);
    }

    @Test
    public void transform() {
        BindyToAvroTreasuryTransformer transformer = new BindyToAvroTreasuryTransformer();
        Treasury t = transformer.transform(treasuryBindy);
        assertNotNull(t);
        assertEquals(1.0, t.getC1year().longValue(),1);
        assertEquals(2.0, t.getC2year().longValue(),1);
        assertEquals(3.0, t.getC10year().longValue(),1);
        assertEquals(4.0, t.getC30year().longValue(),1);
        assertEquals(treasuryDate.getTime(), t.getTimePeriod().longValue());
    }
}