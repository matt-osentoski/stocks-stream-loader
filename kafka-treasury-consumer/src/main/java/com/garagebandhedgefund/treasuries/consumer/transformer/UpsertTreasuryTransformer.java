package com.garagebandhedgefund.treasuries.consumer.transformer;

import com.garagebandhedgefund.trading.model.Treasury;
import com.garagebandhedgefund.treasuries.consumer.repository.TreasuryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpsertTreasuryTransformer {

    @Autowired
    TreasuryRepository treasuryRepository;

    public Treasury transform(Treasury treasury) {
        if (treasury != null && treasury.getTimePeriod() != null) {
            Treasury byDate = treasuryRepository.findByTimePeriod(treasury.getTimePeriod());
            if (byDate != null) {
                treasury.setId(byDate.getId());
            }
        }
        return treasury;
    }
}
