## Kafka Stock Filter Stream
This module uses Kafka Streams to filter a topic with Stock messages.  Only stocks with a
market cap larger than a certain value are sent to a topic for large cap stocks

## Build
mvn clean install

## Kafka information
### Stock Filter Stream
Stocks are retrieved from the 'stocks' topic. A filter operation is performed with
large-cap stocks being placed into the 'stocks-largecap' topic

### Stock Combine Stream
Stocks from the following topics: 'stocks-custom', 'stocks-largecap' are combined into
the 'available-stocks-upsert' topic
