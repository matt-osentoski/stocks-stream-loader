package com.garagebandhedgefund.treasuries.consumer.repository;

import com.garagebandhedgefund.trading.model.Treasury;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface TreasuryRepository extends CrudRepository<Treasury, Long> {
    Treasury findByTimePeriod(Date timePeriod);
}
