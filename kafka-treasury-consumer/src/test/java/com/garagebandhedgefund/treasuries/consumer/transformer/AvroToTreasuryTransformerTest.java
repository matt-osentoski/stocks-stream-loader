package com.garagebandhedgefund.treasuries.consumer.transformer;

import com.garagebandhedgefund.avro.model.Treasury;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AvroToTreasuryTransformerTest {

    private Treasury treasury;
    private Long treasuryDateEpoch;

    @Before
    public void setUp() throws Exception {
        treasuryDateEpoch = new Date().getTime();

        treasury = new Treasury();
        treasury.setC1month(1.1);
        treasury.setC1year(1.2);
        treasury.setC2year(2.0);
        treasury.setC10year(3.0);
        treasury.setC30year(4.0);
        treasury.setTimePeriod(treasuryDateEpoch);

    }

    @Test
    public void transform() {
        AvroToTreasuryTransformer transformer = new AvroToTreasuryTransformer();
        com.garagebandhedgefund.trading.model.Treasury t = transformer.transform(treasury);
        assertNotNull(t);
        assertEquals(1.1, t.getC1month(),1);
        assertEquals(1.2, t.getC1year(),2);
        assertEquals(2.0, t.getC2year(),2);
        assertEquals(3.0, t.getC10year(),2);
        assertEquals(4.0, t.getC30year(),2);
        assertEquals(treasuryDateEpoch, new Long(t.getTimePeriod().getTime()));
    }
}