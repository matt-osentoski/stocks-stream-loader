## CSV Batch loader for Stocks and Treasuries
This module uses Spring Boot/Apache Camel to batch load stock and treasury data from
CSV files.  The data is converted from CSV to Avro objects then placed (ingested) into Kafka topics.  This
Consumer modules then pull the data from the kafka topics and load into Databases or other data stores.

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview  
**csv-batch-loader** module

## Build (including docker images)
```mvn clean install```

## URLs and files used for the batch process

**Stocks Symbols:**  
Stock symbols are gathered from the following CSV file, via NASDAQ:  
~~ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqtraded.txt~~  
https://www.nasdaq.com/screening/company-list.aspx  
Files should be named using the following format:  
companylist_nasdaq.csv  
companylist_nyse.csv  
  
~~More information about the file can be found here:~~ 
~~http://www.nasdaqtrader.com/trader.aspx?id=symboldirdefs~~
  
**Treasuries:**  
The treasury rates CSV file can be found at:  
http://www.federalreserve.gov/datadownload/Output.aspx?rel=H15&series=bf17364827e38702b42a58cf8eaa3f78&lastObs=&from=&to=&filetype=csv&label=include&layout=seriescolumn&type=package  
  
## Kafka information
### Stock topics
Stocks loaded from NASDAQ CSV files are placed into the 'stocks' topic.  
Stocks loaded from the custom-stocks.csv file are placed into the 'stocks-custom' topic.

### Treasury topics
Treasury entries are loaded into the 'treasuries-upsert' topic