package com.garagebandhedgefund.stocks.batchjob.routes;

import com.garagebandhedgefund.stocks.batchjob.bindy.StockBindy;
import com.garagebandhedgefund.stocks.batchjob.transformer.BindyToAvroStockTransformer;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StockRoute extends SpringRouteBuilder {

    @Autowired
    BindyToAvroStockTransformer bindyToAvroStockTransformer;

    @Override
    public void configure() throws Exception {
        DataFormat bindy = new BindyCsvDataFormat(StockBindy.class);

        from("{{csv.trading.directory}}?include=companylist.*\\.csv&move=backup/${date:now:yyyyMMdd}/${file:name}")
                .unmarshal(bindy)
                .split(body())
            .bean(bindyToAvroStockTransformer, "transform")
                .log(LoggingLevel.INFO, "Sending to Kafka: ${body}")
                .to("kafka:{{kafka.stocks.topic}}?brokers={{kafka.brokers.uri}}&serializerClass=org.apache.kafka.common.serialization.StringSerializer");
    //.to("log:out"); // For debugging
}
}
