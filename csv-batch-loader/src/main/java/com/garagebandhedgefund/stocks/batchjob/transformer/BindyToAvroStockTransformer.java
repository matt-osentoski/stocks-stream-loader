package com.garagebandhedgefund.stocks.batchjob.transformer;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.batchjob.bindy.StockBindy;
import com.garagebandhedgefund.stocks.batchjob.utils.StringUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class BindyToAvroStockTransformer {
    final static Logger logger = LoggerFactory.getLogger(BindyToAvroStockTransformer.class);

    public Stock transform(StockBindy stockBindy) {
        Stock stock = new Stock();
        try {
            BeanUtils.copyProperties(stock, stockBindy);
            stock.setMarketCap(StringUtils.marketCapToLong(stockBindy.getMarketCap()));
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(), e);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage(), e);
        }
        return stock;
    }
}
