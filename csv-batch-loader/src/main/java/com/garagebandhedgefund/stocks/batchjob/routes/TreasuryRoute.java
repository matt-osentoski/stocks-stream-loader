package com.garagebandhedgefund.stocks.batchjob.routes;

import com.garagebandhedgefund.stocks.batchjob.bindy.TreasuryBindy;
import com.garagebandhedgefund.stocks.batchjob.processor.TreasuryFileCleanerProcessor;
import com.garagebandhedgefund.stocks.batchjob.transformer.BindyToAvroTreasuryTransformer;
import com.garagebandhedgefund.stocks.batchjob.utils.FileUtilsBean;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TreasuryRoute extends SpringRouteBuilder {

    @Autowired
    BindyToAvroTreasuryTransformer bindyToAvroTreasuryTransformer;

    @Autowired
    @Qualifier("treasuryLineRemovalProcessor")
    TreasuryFileCleanerProcessor treasuryFileCleanerProcessor;

    @Autowired
    FileUtilsBean fileUtilsBean;

    @Override
    public void configure() throws Exception {
        DataFormat bindy = new BindyCsvDataFormat(TreasuryBindy.class);

        from("{{csv.trading.directory}}?fileName={{csv.treasuries.file}}&move=backup/${date:now:yyyyMMdd}/${file:name}")
            .process(treasuryFileCleanerProcessor)
            .unmarshal(bindy)
            .split(body())
            .bean(bindyToAvroTreasuryTransformer, "transform")
            //.log(LoggingLevel.INFO, "Sending to Kafka: ${body}")
            .to("kafka:{{kafka.treasuries.upsert.topic}}?brokers={{kafka.brokers.uri}}&serializerClass=org.apache.kafka.common.serialization.StringSerializer")
            .bean(fileUtilsBean, "deleteFile(${header.tempFile})");
    }
}
