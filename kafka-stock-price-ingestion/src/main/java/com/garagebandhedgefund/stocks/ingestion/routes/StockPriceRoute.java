package com.garagebandhedgefund.stocks.ingestion.routes;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.ingestion.bindy.StockPriceBindy;
import com.garagebandhedgefund.stocks.ingestion.transformer.BindyToAvroStockPriceTransformer;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This route pulls all historical daily pricing for stocks in the 'available stocks' topic.
 * The purpose of this route is to continuously maintain the list of stock prices for a symbol, beyond the initial batch load.
 */
@Component
public class StockPriceRoute extends SpringRouteBuilder {

    @Autowired
    private BindyToAvroStockPriceTransformer bindyToAvroStockPriceTransformer;

    @Override
    public void configure() throws Exception {
        DataFormat bindy = new BindyCsvDataFormat(StockPriceBindy.class);
        //
        // Grab Stocks from kafka
        from("kafka:{{kafka.available.stocks.upsert.topic}}?brokers={{kafka.brokers.uri}}&groupId={{kafka.stock.find.prices.group.id}}&autoOffsetReset=earliest&consumersCount=1&valueDeserializer=org.apache.kafka.common.serialization.StringDeserializer")
                .unmarshal().json(JsonLibrary.Jackson, Stock.class)
                .setHeader("symbol", simple("${body.symbol}")) // Save the symbol into the header for use later.
                .log(LoggingLevel.INFO, "Stock symbol: ${header.symbol}")
                //
                //Make a REST call to retrieve stock prices
                .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.GET))
                .setHeader(Exchange.CONTENT_TYPE, constant("text/csv"))
                .setBody(constant(""))  // Reset the body to empty, so that the Avro message isn't sent to the REST API
                .toD("https4://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${header.symbol}&outputsize=full&apikey={{api.key.alphavantage}}&datatype=csv")
                //
                // Split the stock price array, transform into AVRO objects, then send to Kafka.
                .unmarshal(bindy)
                .split(body())
                    .bean(bindyToAvroStockPriceTransformer, "transform(${body}, ${header.symbol})")
     //               .log(LoggingLevel.INFO, "Sending to Kafka: ${body}")
                    .to("kafka:{{kafka.stock.prices.upsert.topic}}?brokers={{kafka.brokers.uri}}&serializerClass=org.apache.kafka.common.serialization.StringSerializer");
    }
}
