## Kafka Stock Price Ingestion
This module pushes stock prices into Kafka.  This is performed at both a batch and streaming level.
Prices are only pulled for Stocks that have been persisted into a database. These stocks are sourced
from the 'available-stocks-upsert' topic in Kafka.  
  
Stocks are pushed into Kafka in three timeframes:  
1. Historical End of Day (EOD) stock prices.  (Stream Operation)
2. End of current day prices. (Cron-based batch operation)
3. Intraday prices pulled at intervals throughout the day. (Cron-based batch operation

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview  
**kafka-stock-price-ingestion** module

## Build (including docker images)
```mvn clean install```

## Kafka information
###Historical EOD prices
Historical stock prices are pull from a Web Service then transformed and the prices pushed into
the 'stock-prices-upsert' topic

###End of current day prices
Using a cronjob, the EOD price is pushed to the 'stock-prices-upsert' topic at the end of the trading day

###Intraday stock prices
Using a cronjob, prices throughout the trading day are pushed to the 'stock-price-intraday' topic


  
