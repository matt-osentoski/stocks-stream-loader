package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.stocks.consumer.repository.StockRepository;
import com.garagebandhedgefund.trading.model.Stock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpsertStockTransformerTest {

    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private UpsertStockTransformer upsertStockTransformer;

    private Stock stock;

    @Before
    public void setUp() throws Exception {
        stock = new Stock();
        stock.setSymbol("AAPL");
        stock.setName("Apple");
        stock.setMarketCap(100000000L);
    }

    @Test
    public void transform() {
        when(stockRepository.findBySymbol("AAPL")).thenReturn(stock);
        Stock s = upsertStockTransformer.transform(stock);
        assertNotNull(s);
        assertEquals(0, s.getId());
        assertEquals("AAPL", s.getSymbol());
        assertEquals("Apple", s.getName());
        assertEquals(100000000L, s.getMarketCap(),0);
    }
}