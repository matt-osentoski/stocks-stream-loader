package com.garagebandhedgefund.stocks.consumer.routes;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.consumer.transformer.AvroToStockTransformer;
import com.garagebandhedgefund.stocks.consumer.transformer.UpsertStockTransformer;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StockRoute extends SpringRouteBuilder {

    @Autowired
    AvroToStockTransformer avroToStockTransformer;

    @Autowired
    UpsertStockTransformer upsertStockTransformer;

    @Override
    public void configure() throws Exception {

        from("kafka:{{kafka.available.stocks.upsert.topic}}?brokers={{kafka.brokers.uri}}&autoOffsetReset=earliest&consumersCount=1&valueDeserializer=org.apache.kafka.common.serialization.StringDeserializer")
                .unmarshal().json(JsonLibrary.Jackson, Stock.class)
                .bean(avroToStockTransformer, "transform")
                .bean(upsertStockTransformer, "transform")
                .to("jpa:com.garagebandhedgefund.trading.model.Stock?");
    }
}
