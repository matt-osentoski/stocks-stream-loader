package com.garagebandhedgefund.stocks.ingestion.routes;

import com.garagebandhedgefund.stocks.ingestion.model.alphavantage.BatchStockQuotes;
import com.garagebandhedgefund.stocks.ingestion.transformer.JsonToAvroBatchStockQuoteTransformer;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This route uses a scheduler to pull the current end of day (EOD) stock prices down from a web service.
 * This keeps the daily stock prices up to date, beyond the initial CSV batched-based ETL load.
 *
 * This route differs from StockPriceRoute in that it only pulls down one price per day, per symbol, whereas the
 * StockPriceRoute will pull down all historical pricing.
 */
@Component
public class IntradayStockPriceRoute extends SpringRouteBuilder {

    @Autowired
    private JsonToAvroBatchStockQuoteTransformer jsonToAvroBatchStockQuoteTransformer;

    @Override
    public void configure() throws Exception {

        from("quartz2://intradayTimer?cron={{cron.job.intraday.stock.prices}}")
                //
                // Update all available stock symbols from JPA
                .pollEnrich("jpa://com.garagebandhedgefund.trading.model.Stock?consumer.query=SELECT o FROM com.garagebandhedgefund.trading.model.Stock o")
                .split(body())
                .setHeader("symbol", simple("${body.symbol}"))
                //
                //Make a REST call to retrieve the Intraday stock price
                .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.GET))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .setBody(constant(""))  // Reset the body to empty, so that the Avro message isn't sent to the REST API
                .toD("https4://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=${header.symbol}&apikey={{api.key.alphavantage}}")
                //
                // transform into AVRO objects, then send to Kafka.
                .unmarshal().json(JsonLibrary.Jackson, BatchStockQuotes.class)
                .bean(jsonToAvroBatchStockQuoteTransformer, "transform(${body}, ${header.symbol})")
                .log(LoggingLevel.INFO, "Sending to Kafka: ${body}")
                .to("kafka:{{kafka.stock.prices.intraday.topic}}?brokers={{kafka.brokers.uri}}&serializerClass=org.apache.kafka.common.serialization.StringSerializer");
                //.to("log:out"); // For debugging
    }
}
