## Sample CSV files
The files in this directory contain samples of the files that the 'csv-batch-loader'
module is expecting:

**companylist_nasdaq.csv**   
This file comes from Nasdaq and contains a list of 
most major stock symbols.  ETF, ETN, and other specialized instruments are excluded from this file.  
(Source: https://www.nasdaq.com/screening/company-list.aspx)  
  
**FRB_H15.csv**  
This file contains treasury interest rates.  
(Source: http://www.federalreserve.gov/datadownload/Output.aspx?rel=H15&series=bf17364827e38702b42a58cf8eaa3f78&lastObs=&from=&to=&filetype=csv&label=include&layout=seriescolumn&type=package)  
  
**custom-stocks.csv**  
This is a custom CSV file that's used for loading symbols that are not contained in the NASDAQ sourced files.
For example: ETF's, ETN's, exchange symbols, or penny stocks.  This file is manually
populated to contain symbols of interest.