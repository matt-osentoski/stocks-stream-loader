package com.garagebandhedgefund.stocks.ingestion.transformer;

import com.garagebandhedgefund.avro.model.StockPrice;
import com.garagebandhedgefund.stocks.ingestion.bindy.StockPriceBindy;
import com.garagebandhedgefund.stocks.ingestion.model.iex.TradingStockPrice;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class BindyToAvroStockPriceTransformerTest {

    private StockPriceBindy source;
    private Date now;

    @Before
    public void setUp() throws Exception {
        now = new Date();
        source = new StockPriceBindy();
        source.setClose(2.50);
        source.setTimestamp(now);
        source.setHigh(3.14);
        source.setLow(2.39);
        source.setOpen(3.01);
        source.setVolume(1000000000);
    }

    @Test
    public void transform() {
        BindyToAvroStockPriceTransformer transformer = new BindyToAvroStockPriceTransformer();
        StockPrice sp = transformer.transform(source, "AAPL");
        assertNotNull(sp);
        assertEquals("AAPL", sp.getSymbol());
        assertEquals(2.50, sp.getClosingPrice(),0);
        assertEquals(3.14, sp.getHighPrice(),0);
        assertEquals(2.39, sp.getLowPrice(),0);
        assertEquals(3.01, sp.getOpeningPrice(),0);
        assertEquals(now.getTime(), sp.getPriceDate().longValue());
        assertEquals(1000000000L, sp.getVolume(), 0);

        // a few negative tests
        assertNotEquals(1.50, sp.getClosingPrice(),0);
        assertNotEquals(0.14, sp.getHighPrice(),0);
        assertNotEquals(0.39, sp.getLowPrice(),0);
    }
}