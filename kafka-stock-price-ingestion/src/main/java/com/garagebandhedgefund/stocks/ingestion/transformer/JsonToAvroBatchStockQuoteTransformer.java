package com.garagebandhedgefund.stocks.ingestion.transformer;

import com.garagebandhedgefund.avro.model.StockQuote;
import com.garagebandhedgefund.stocks.ingestion.model.alphavantage.BatchStockQuotes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JsonToAvroBatchStockQuoteTransformer {
    final static Logger logger = LoggerFactory.getLogger(JsonToAvroBatchStockQuoteTransformer.class);

    public StockQuote transform(BatchStockQuotes batchStockQuotes, String symbol) {
        StockQuote quote = null;
        List<com.garagebandhedgefund.stocks.ingestion.model.alphavantage.StockQuote> stockQuotes =
                batchStockQuotes.getStockQuotes();
        if (stockQuotes != null && stockQuotes.size() > 0) {
            quote = new StockQuote();
            com.garagebandhedgefund.stocks.ingestion.model.alphavantage.StockQuote stockQuote = stockQuotes.get(0);
            quote.setPrice(stockQuote.getPrice());
            quote.setSymbol(symbol);
            quote.setTimestamp(stockQuote.getTimestamp().getTime());
        }
        return quote;
    }
}
