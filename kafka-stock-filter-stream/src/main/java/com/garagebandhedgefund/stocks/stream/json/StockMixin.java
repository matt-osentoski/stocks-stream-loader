package com.garagebandhedgefund.stocks.stream.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.avro.Schema;

public abstract class StockMixin {
    @JsonIgnore abstract Schema getSchema();
}
