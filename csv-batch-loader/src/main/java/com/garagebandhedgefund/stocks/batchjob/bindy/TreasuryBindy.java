package com.garagebandhedgefund.stocks.batchjob.bindy;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import java.util.Date;

@CsvRecord( separator = "," )
public class TreasuryBindy {

    @DataField(pos = 1, pattern = "yyyy-MM-dd")
    private Date timePeriod;

    @DataField(pos = 2, trim = true, defaultValue = "0")
    private double c1month;

    @DataField(pos = 3, trim = true, defaultValue = "0")
    private double c3month;

    @DataField(pos = 4, trim = true, defaultValue = "0")
    private double c6month;

    @DataField(pos = 5, trim = true, defaultValue = "0")
    private double c1year;

    @DataField(pos = 6, trim = true, defaultValue = "0")
    private double c2year;

    @DataField(pos = 7, trim = true, defaultValue = "0")
    private double c3year;

    @DataField(pos = 8, trim = true, defaultValue = "0")
    private double c5year;

    @DataField(pos = 9, trim = true, defaultValue = "0")
    private double c7year;

    @DataField(pos = 10, trim = true, defaultValue = "0")
    private double c10year;

    @DataField(pos = 11, trim = true, defaultValue = "0")
    private double c20year;

    @DataField(pos = 12, trim = true, defaultValue = "0")
    private double c30year;

    public Date getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Date timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getC1month() {
        return c1month;
    }

    public void setC1month(double c1month) {
        this.c1month = c1month;
    }

    public double getC3month() {
        return c3month;
    }

    public void setC3month(double c3month) {
        this.c3month = c3month;
    }

    public double getC6month() {
        return c6month;
    }

    public void setC6month(double c6month) {
        this.c6month = c6month;
    }

    public double getC1year() {
        return c1year;
    }

    public void setC1year(double c1year) {
        this.c1year = c1year;
    }

    public double getC2year() {
        return c2year;
    }

    public void setC2year(double c2year) {
        this.c2year = c2year;
    }

    public double getC3year() {
        return c3year;
    }

    public void setC3year(double c3year) {
        this.c3year = c3year;
    }

    public double getC5year() {
        return c5year;
    }

    public void setC5year(double c5year) {
        this.c5year = c5year;
    }

    public double getC7year() {
        return c7year;
    }

    public void setC7year(double c7year) {
        this.c7year = c7year;
    }

    public double getC10year() {
        return c10year;
    }

    public void setC10year(double c10year) {
        this.c10year = c10year;
    }

    public double getC20year() {
        return c20year;
    }

    public void setC20year(double c20year) {
        this.c20year = c20year;
    }

    public double getC30year() {
        return c30year;
    }

    public void setC30year(double c30year) {
        this.c30year = c30year;
    }
}
