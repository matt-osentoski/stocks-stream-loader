package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.stocks.consumer.repository.StockPriceRepository;
import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpsertStockPriceTransformer {

    @Autowired
    StockPriceRepository stockPriceRepository;

    public StockPrice transform(StockPrice stockPrice) {
        if (stockPrice != null) {
            StockPrice sp =
                    stockPriceRepository.findBySymbolAndPriceDate(stockPrice.getSymbol(), stockPrice.getPriceDate());
            if (sp != null) {
                stockPrice.setId(sp.getId());
            }
        }
        return stockPrice;
    }
}
