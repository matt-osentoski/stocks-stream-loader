package com.garagebandhedgefund.stocks.batchjob.routes;

import com.garagebandhedgefund.stocks.batchjob.bindy.CustomStockBindy;
import com.garagebandhedgefund.stocks.batchjob.transformer.BindyToAvroCustomStockTransformer;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomStockRoute extends SpringRouteBuilder {

    @Autowired
    BindyToAvroCustomStockTransformer bindyToAvroCustomStockTransformer;

    @Override
    public void configure() throws Exception {
        DataFormat bindy = new BindyCsvDataFormat(CustomStockBindy.class);

        from("{{csv.trading.directory}}?include={{csv.custom.stocks.filename}}&move=backup/${date:now:yyyyMMdd}/${file:name}")
                .unmarshal(bindy)
                .split(body())
                .bean(bindyToAvroCustomStockTransformer, "transform")
                .log(LoggingLevel.INFO, "Sending to Kafka: ${body}")
                .to("kafka:{{kafka.stocks.custom.topic}}?brokers={{kafka.brokers.uri}}&serializerClass=org.apache.kafka.common.serialization.StringSerializer");
    }
}
