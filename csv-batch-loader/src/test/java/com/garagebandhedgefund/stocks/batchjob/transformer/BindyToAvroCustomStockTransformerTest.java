package com.garagebandhedgefund.stocks.batchjob.transformer;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.batchjob.bindy.CustomStockBindy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BindyToAvroCustomStockTransformerTest {

    private CustomStockBindy stockBindy;

    @Before
    public void setUp() throws Exception {
        stockBindy = new CustomStockBindy();
        stockBindy.setSymbol("AAPL");
        stockBindy.setMarketCap("100M");
        stockBindy.setName("Apple");
        stockBindy.setIsIndex("0");
        stockBindy.setIsETF("0");
        stockBindy.setEtfLeverageMultiplier("0");
        stockBindy.setIsShortEtf("0");
    }

    @Test
    public void transform() {
        BindyToAvroCustomStockTransformer transformer = new BindyToAvroCustomStockTransformer();
        Stock s = transformer.transform(stockBindy);
        assertNotNull(s);
        assertEquals("AAPL", s.getSymbol());
        assertEquals(100000000L, s.getMarketCap().longValue());
        assertEquals("Apple", s.getName());
        assertEquals(0, s.getIsEtf().longValue());
    }
}