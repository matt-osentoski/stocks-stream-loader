package com.garagebandhedgefund.stocks.batchjob.transformer;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.batchjob.bindy.CustomStockBindy;
import com.garagebandhedgefund.stocks.batchjob.utils.StringUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class BindyToAvroCustomStockTransformer {
    final static Logger logger = LoggerFactory.getLogger(BindyToAvroCustomStockTransformer.class);

    public Stock transform(CustomStockBindy stockBindy) {
        Stock stock = new Stock();
        try {
            BeanUtils.copyProperties(stock, stockBindy);
            stock.setMarketCap(StringUtils.marketCapToLong(stockBindy.getMarketCap()));
            stock.setIsIndex(Integer.parseInt(stockBindy.getIsIndex()));
            stock.setIsEtf(Integer.parseInt(stockBindy.getIsETF()));
            stock.setIsShortEtf(Integer.parseInt(stockBindy.getIsShortEtf()));
            stock.setEtfLeverageMultiplier(Double.parseDouble(stockBindy.getEtfLeverageMultiplier()));
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(), e);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage(), e);
        }
        return stock;
    }

}
