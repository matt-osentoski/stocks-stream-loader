package com.garagebandhedgefund.treasuries.consumer.routes;

import com.garagebandhedgefund.avro.model.Treasury;
import com.garagebandhedgefund.treasuries.consumer.transformer.AvroToTreasuryTransformer;
import com.garagebandhedgefund.treasuries.consumer.transformer.UpsertTreasuryTransformer;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TreasuryRoute extends SpringRouteBuilder {

    @Autowired
    AvroToTreasuryTransformer avroToTreasuryTransformer;

    @Autowired
    UpsertTreasuryTransformer upsertTreasuryTransformer;

    @Override
    public void configure() throws Exception {
        from("kafka:{{kafka.treasuries.upsert.topic}}?brokers={{kafka.brokers.uri}}&autoOffsetReset=earliest&consumersCount=1&valueDeserializer=org.apache.kafka.common.serialization.StringDeserializer")
                //.log(LoggingLevel.INFO, "Received from Kafka: ${body}")
                .unmarshal().json(JsonLibrary.Jackson, Treasury.class)
                //.unmarshal().avro(Stock.getClassSchema())
                .bean(avroToTreasuryTransformer, "transform")
                .bean(upsertTreasuryTransformer, "transform")
                .to("jpa:com.garagebandhedgefund.trading.model.Treasury?");
    }
}
