# Stock Streaming Loader
This project contains modules for loading stock information and pricing using a combination
of ETL and stream processing.  Kafka will be used as the pipeline for stream prcoessing.

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview

## Build all modules (including docker images)
```mvn clean install```

## Deployments

### Docker Compose
(NOTE: If you use Docker compose, you can skip the section on Kubernetes isntall using Helm below)
```docker-compose up -d --no-build```
  
For windows you might have to run the following commands:  
```
set COMPOSE_CONVERT_WINDOWS_PATHS=1
docker-compose up -d --no-build
```
(NOTE: make sure the images are built using Maven and the --no-build flag
is used above.  )  
(Windows help:  In windows, you might have to run the docker-compose command from a docker shell)


### Kubernetes install using Helm
To deploy to Kubernetes using Helm/Tiller, run the following commands below:

>(NOTE: You will have to have Docker with Kubernetes already running on your computer and have Helm installed.
You will also have to setup a docker registry server available to your Kubernetes cluster and change the Helm
template file `repository` settings to reflect this server location)

```
cd charts/stocks-stream-loader
helm install . --name stocks-stream-loader

# To redeploy using the same release name:
helm ls --all stocks-stream-loader
helm del --purge stocks-stream-loader
helm install . --name stocks-stream-loader
```

## Kafka information
Make sure Kafka is running before running any of the modules below.  
(NOTE: Check the configurations for each project, to ensure they have the correct Kafka connection strings, etc.)
  
The following topics should already be created:  
- stocks 
- stock-prices-upsert  
- treasuries-upsert  
- stock-fundamentals-upsert  
- stocks-largecap  
- stocks-custom 
- available-stocks-upsert  
- stock-price-intraday  

## Cloud configuration
Some of the modules use Spring Cloud Config for secure configuration (i.e. configurations that will not
be stored in GIT)  
For example, the 'kafka-stock-price-ingestion' module uses a spring cloud property for a 3rd party
API key, used for pulling down stock quotes.

## Modules
###avro-schemas
This module creates java classes from Avro *.avsc files that will be used for the batch/stream
processing operations. 

###csv-batch-loader
This module uses Spring Boot/Apache Camel to batch load stock and treasury data from
CSV files.  The data is converted from CSV to Avro objects then placed (ingested) into Kafka topics.  This
Consumer modules then pull the data from the kafka topics and load into Databases or other data stores.

###kafka-stock-consumer
This module uses Spring Boot/Apache Camel to pull Stocks from Kafka and updates a database with the entries.  Other datasources can
be written to in the future.

###kafka-stock-filter-stream
This module uses Kafka Streams to filter a topic with Stock messages.  Only stocks with a
market cap larger than a certain value are sent to a topic for large cap stocks

###kafka-stock-price-consumer
This module uses Spring Boot/Apache Camel to pull Stock prices from Kafka and updates a database with the entries.  Other datasources can
be written to in the future.

###kafka-stock-price-ingestion
This module pushes stock prices into Kafka.  This is performed at both a batch and streaming level.

###kafka-treasury-consumer
This module uses Spring Boot/Apache Camel to pull Treasuries from Kafka and updates a database with the entries.  Other datasources can
be written to in the future.
  


