package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.stocks.consumer.repository.StockPriceRepository;
import com.garagebandhedgefund.trading.model.StockPrice;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpsertStockPriceTransformerTest {

    @Mock
    private StockPriceRepository stockPriceRepository;

    @InjectMocks
    private UpsertStockPriceTransformer upsertStockPriceTransformer;

    private StockPrice stockPrice;
    private Date now;

    @Before
    public void setUp() throws Exception {
        now = new Date();
        stockPrice = new StockPrice();
        stockPrice.setSymbol("AAPL");
        stockPrice.setPeriod("d");
        stockPrice.setClosingPrice(3.14);
        stockPrice.setPriceDate(now);
    }

    @Test
    public void transform() {
        // No primary key
        when(stockPriceRepository.findBySymbolAndPriceDate("AAPL", now)).thenReturn(stockPrice);
        StockPrice sp = upsertStockPriceTransformer.transform(stockPrice);
        assertNotNull(sp);
        assertEquals(0, sp.getId());

        sp.setId(1);
        when(stockPriceRepository.findBySymbolAndPriceDate("AAPL", now)).thenReturn(stockPrice);
        sp = upsertStockPriceTransformer.transform(stockPrice);
        assertEquals(1, sp.getId());

    }
}