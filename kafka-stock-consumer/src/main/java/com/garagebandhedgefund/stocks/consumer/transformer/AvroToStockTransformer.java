package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.avro.model.Stock;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class AvroToStockTransformer {
    final static Logger logger = LoggerFactory.getLogger(AvroToStockTransformer.class);

    public com.garagebandhedgefund.trading.model.Stock transform(Stock avroStock) {
        com.garagebandhedgefund.trading.model.Stock stock = new com.garagebandhedgefund.trading.model.Stock();
        try {
            BeanUtils.copyProperties(stock, avroStock);
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(), e);
        }
        return stock;
    }
}
