package com.garagebandhedgefund.stocks.ingestion.routes;

import com.garagebandhedgefund.stocks.ingestion.bindy.StockPriceBindy;
import com.garagebandhedgefund.stocks.ingestion.model.iex.Quote;
import com.garagebandhedgefund.stocks.ingestion.transformer.BindyToAvroStockPriceTransformer;
import com.garagebandhedgefund.stocks.ingestion.transformer.JsonToAvroQuoteTransformer;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This route uses a scheduler to pull the current end of day (EOD) stock prices down from a web service.
 * This keeps the daily stock prices up to date, beyond the initial CSV batched-based ETL load.
 *
 * This route differs from StockPriceRoute in that it only pulls down one price per day, per symbol, whereas the
 * StockPriceRoute will pull down all historical pricing.
 */
@Component
public class EodStockPriceRoute extends SpringRouteBuilder {

    @Autowired
    private BindyToAvroStockPriceTransformer bindyToAvroStockPriceTransformer;

    @Override
    public void configure() throws Exception {
        DataFormat bindy = new BindyCsvDataFormat(StockPriceBindy.class);

        from("quartz2://eodTimer?cron={{cron.job.eod.stock.prices}}")
                //
                // Update all available stock symbols
                .pollEnrich("jpa://com.garagebandhedgefund.trading.model.Stock?consumer.query=SELECT o FROM com.garagebandhedgefund.trading.model.Stock o")
                .split(body())
                .log(LoggingLevel.INFO, "Starting EOD process ${body.symbol}")
                .setHeader("symbol", simple("${body.symbol}")) // Save the symbol into the header for use later.
                //
                //Make a REST call to retrieve the EOD stock price
                .setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.GET))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .setBody(constant(""))  // Reset the body to empty, so that the Avro message isn't sent to the REST API
                .toD("https4://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${header.symbol}&outputsize=compact&apikey={{api.key.alphavantage}}&datatype=csv")
                //
                // Split the stock price array, transform into AVRO objects, then send to Kafka.
                .unmarshal(bindy)
                .split(body()).filter().simple("${property.CamelSplitIndex} == 0")  // Process only the first line
                    .bean(bindyToAvroStockPriceTransformer, "transform(${body}, ${header.symbol})")
                .log(LoggingLevel.INFO, "Sending to Kafka: ${body}")
                .to("kafka:{{kafka.stock.prices.upsert.topic}}?brokers={{kafka.brokers.uri}}&serializerClass=org.apache.kafka.common.serialization.StringSerializer");

    }
}
