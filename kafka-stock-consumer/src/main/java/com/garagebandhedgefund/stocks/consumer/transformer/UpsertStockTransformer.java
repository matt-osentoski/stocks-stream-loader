package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.stocks.consumer.repository.StockRepository;
import com.garagebandhedgefund.trading.model.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpsertStockTransformer {

    @Autowired
    StockRepository stockRepository;

    public Stock transform(Stock stock) {
        if (stock != null && stock.getSymbol() != null) {
            Stock bySymbol = stockRepository.findBySymbol(stock.getSymbol());
            if (bySymbol != null) {
                stock.setId(bySymbol.getId());
            }
        }
        return stock;
    }
}
