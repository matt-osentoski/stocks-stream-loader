## Kafka Stock Price Consumer
This module uses Spring Boot/Apache Camel to pull Stock prices from Kafka and updates a database with the entries.  Other datasources can
be written to in the future.

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview  
**kafka-stock-price-consumer** module

## Build (including docker images)
```mvn clean install```

## Kafka information
Stock prices are pulled from the 'stock-prices-upsert' topic, then loaded into a database.
  
(NOTE: This module uses a Kafka group, allow multiple instances to be run, allowing clustered processing.)