package com.garagebandhedgefund.stocks.batchjob.bindy;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * This bindy is used to Map CSV files from Nasdaq
 */
@CsvRecord( separator = ",", skipFirstLine = true )
public class StockBindy {

    @DataField(pos = 1, trim = true)
    private String symbol;

    @DataField(pos = 2, trim = true)
    private String name;

    @DataField(pos = 3, trim = true)
    private String lastSale;

    @DataField(pos = 4, trim = true)
    private String marketCap;

    @DataField(pos = 5, trim = true)
    private String ipoYear;

    @DataField(pos = 6, trim = true)
    private String sector;

    @DataField(pos = 7, trim = true)
    private String industry;

    @DataField(pos = 8, trim = true)
    private String summaryQuote;

    @DataField(pos = 9)
    private String empty;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastSale() {
        return lastSale;
    }

    public void setLastSale(String lastSale) {
        this.lastSale = lastSale;
    }

    public String getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public String getIpoYear() {
        return ipoYear;
    }

    public void setIpoYear(String ipoYear) {
        this.ipoYear = ipoYear;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSummaryQuote() {
        return summaryQuote;
    }

    public void setSummaryQuote(String summaryQuote) {
        this.summaryQuote = summaryQuote;
    }

    public String getEmpty() {
        return empty;
    }

    public void setEmpty(String empty) {
        this.empty = empty;
    }
}
