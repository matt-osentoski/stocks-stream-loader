package com.garagebandhedgefund.stocks.batchjob.utils;

public class StringUtils {
    public static long marketCapToLong(String marketCap) throws NumberFormatException {
        long marketCapValue = 0;
        if (!"".equals(marketCap) && !("n/a".equals(marketCap))) {
            marketCap = marketCap.replace("$", "");
            if (marketCap.contains("B")) {
                marketCap = marketCap.replace("B", "");
                double marketCapDouble = Double.parseDouble(marketCap);
                marketCapValue = (long) (marketCapDouble * 1000000000);
            } else if (marketCap.contains("M")) {
                marketCap = marketCap.replace("M", "");
                double marketCapDouble = Double.parseDouble(marketCap);
                marketCapValue = (long) (marketCapDouble * 1000000);
            } else {
                double marketCapDouble = Double.parseDouble(marketCap);
                marketCapValue = (long) marketCapDouble;
            }
        }
        return marketCapValue;
    }
}
