package com.garagebandhedgefund.stocks.consumer.repository;

import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface StockPriceRepository extends CrudRepository<StockPrice, Long> {
    StockPrice findBySymbolAndPriceDate(String symbol, Date priceDate);
}
