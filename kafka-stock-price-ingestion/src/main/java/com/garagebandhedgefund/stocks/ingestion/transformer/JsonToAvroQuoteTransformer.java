package com.garagebandhedgefund.stocks.ingestion.transformer;

import com.garagebandhedgefund.avro.model.StockPrice;
import com.garagebandhedgefund.stocks.ingestion.model.iex.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JsonToAvroQuoteTransformer {
    final static Logger logger = LoggerFactory.getLogger(JsonToAvroQuoteTransformer.class);

    public StockPrice transform(Quote sourceQuote, String symbol) {
        StockPrice stockPrice = new StockPrice();
        stockPrice.setSymbol(symbol);
        stockPrice.setClosingPrice(sourceQuote.getClose());
        stockPrice.setHighPrice(sourceQuote.getHigh());
        stockPrice.setLowPrice(sourceQuote.getLow());
        stockPrice.setOpeningPrice(sourceQuote.getOpen());
        stockPrice.setPriceDate(sourceQuote.getCloseTime());
        stockPrice.setVolume(sourceQuote.getLatestVolume());
        stockPrice.setPeriod("d");
        return stockPrice;
    }
}
