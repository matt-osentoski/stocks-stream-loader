package com.garagebandhedgefund.stocks.consumer.repository;

import com.garagebandhedgefund.trading.model.Stock;
import org.springframework.data.repository.CrudRepository;

public interface StockRepository extends CrudRepository<Stock, Long> {
    Stock findBySymbol(String symbol);
}
