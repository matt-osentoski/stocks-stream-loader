## Kafka Treasury Consumer
This module uses Spring Boot/Apache Camel to pull Treasuries from Kafka and updates a database with the entries.  Other datasources can
be written to in the future.

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview  
**kafka-treasury-consumer** module

## Build (including docker images)
```mvn clean install```

## Kafka information
Treasury values are pulled from the treasuries-upsert' topic, then loaded into a database.