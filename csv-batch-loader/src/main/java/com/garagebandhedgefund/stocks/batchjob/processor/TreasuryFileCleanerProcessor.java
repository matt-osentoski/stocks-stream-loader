package com.garagebandhedgefund.stocks.batchjob.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class TreasuryFileCleanerProcessor implements Processor {
    final static Logger logger = LoggerFactory.getLogger(TreasuryFileCleanerProcessor.class);

    private int lines;

    public TreasuryFileCleanerProcessor(int lines) {
        this.lines = lines;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Scanner sc = null;
        PrintWriter printer = null;
        try {
            Message message = exchange.getIn();
            File fileToProcess = message.getBody(File.class);
            logger.info("File detected: '" + fileToProcess.getAbsolutePath());

            File output = new File(fileToProcess.getPath() + ".tmp");
            sc = new Scanner(fileToProcess);
            printer = new PrintWriter(output);
            int c = 0;
            while (sc.hasNextLine()) {
                c++;
                if (c <= this.lines) {
                    sc.nextLine();
                    continue;
                }
                String s = sc.nextLine();

                // Some entries have no data, marked as 'ND'.  This is messing up the data binder.
                // These entries should be cleared out
                s = s.replaceAll("ND", "");

                printer.write(s + "\r\n");
            }
            printer.flush();
            exchange.getIn().setBody(output);
            exchange.getIn().setHeader("tempFile", output.getAbsolutePath());

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            sc.close();
            printer.close();
        }
    }
}
