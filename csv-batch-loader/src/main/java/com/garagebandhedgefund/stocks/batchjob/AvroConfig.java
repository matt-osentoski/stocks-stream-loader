package com.garagebandhedgefund.stocks.batchjob;

import com.garagebandhedgefund.stocks.batchjob.processor.TreasuryFileCleanerProcessor;
import org.apache.avro.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class AvroConfig {
    final static Logger logger = LoggerFactory.getLogger(AvroConfig.class);

    @Bean(name = "treasuryLineRemovalProcessor")
    public TreasuryFileCleanerProcessor removalProcessor() {
        return new TreasuryFileCleanerProcessor(6);
    }

    private Schema getSchema(String avscPath) {
        InputStream schemaStream = getClass().getResourceAsStream(avscPath);
        Schema stockSchema = null;
        try {
            stockSchema = new Schema.Parser().parse(schemaStream);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return stockSchema;
    }
}
