package com.garagebandhedgefund.stocks.consumer.routes;

import com.garagebandhedgefund.avro.model.StockPrice;
import com.garagebandhedgefund.stocks.consumer.transformer.AvroToStockPriceTransformer;
import com.garagebandhedgefund.stocks.consumer.transformer.UpsertStockPriceTransformer;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StockPriceRoute extends SpringRouteBuilder {

    @Autowired
    private AvroToStockPriceTransformer avroToStockPriceTransformer;

    @Autowired
    private UpsertStockPriceTransformer upsertStockPriceTransformer;

    @Override
    public void configure() throws Exception {
        from("kafka:{{kafka.stock.prices.upsert.topic}}?brokers={{kafka.brokers.uri}}&groupId={{kafka.stock.prices.group.id}}&autoOffsetReset=earliest&consumersCount=1&valueDeserializer=org.apache.kafka.common.serialization.StringDeserializer")
                .unmarshal().json(JsonLibrary.Jackson, StockPrice.class)
                .bean(avroToStockPriceTransformer, "transform")
                .bean(upsertStockPriceTransformer, "transform")
                .to("jpa:com.garagebandhedgefund.trading.model.StockPrice?");
    }
}
