package com.garagebandhedgefund.stocks.consumer.transformer;

import com.garagebandhedgefund.avro.model.Stock;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AvroToStockTransformerTest {

    private Stock avroStock;

    @Before
    public void setUp() throws Exception {
        avroStock = new Stock();
        avroStock.setSymbol("AAPL");
        avroStock.setName("Apple");
        avroStock.setMarketCap(100000000L);
    }

    @Test
    public void transform() {
        AvroToStockTransformer transformer = new AvroToStockTransformer();
        com.garagebandhedgefund.trading.model.Stock s = transformer.transform(avroStock);
        assertNotNull(s);
        assertEquals("AAPL", s.getSymbol());
        assertEquals("Apple", s.getName());
        assertEquals(100000000L, s.getMarketCap(),0);
    }
}