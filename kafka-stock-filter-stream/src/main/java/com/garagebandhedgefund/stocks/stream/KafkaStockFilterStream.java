package com.garagebandhedgefund.stocks.stream;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.stream.serdes.JsonPOJODeserializer;
import com.garagebandhedgefund.stocks.stream.serdes.JsonPOJOSerde;
import com.garagebandhedgefund.stocks.stream.serdes.JsonPOJOSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * This class filters a stock-upsert topic by the market cap. size.
 *
 * This class was derived from an example on github:
 * https://github.com/jkorab/kafka-streams-example
 */
@Component
public class KafkaStockFilterStream {

    @Value("${kafka.brokers.uri}")
    private String brokerUris;

    @Value("${kafka.stocks.topic}")
    private String topic;

    @Value("${kafka.streams.filter.application.id}")
    private String applicationId;

    @Value("${kafka.stocks.largecap.topic}")
    private String largecapTopic;

    @Value("${kafka.filter.minimum.marketcap}")
    private Long minimumMarketcap;

    private KafkaStreams streams;

    @PostConstruct
    public void runStream() {
        Serde<String> stringSerde = Serdes.String();
        Map<String, Object> serdeProps = new HashMap<>();

        final Serializer<Stock> stockSerializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", Stock.class);
        stockSerializer.configure(serdeProps, false);
        final Deserializer<Stock> stockDeserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", Stock.class);
        stockDeserializer.configure(serdeProps, false);

        final Serde<Stock> jsonSerde = Serdes.serdeFrom(stockSerializer, stockDeserializer);

        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, brokerUris);

        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        KStreamBuilder builder = new KStreamBuilder();
        builder.stream(stringSerde, jsonSerde, topic)
                .filter((key, value) -> value.getMarketCap() > minimumMarketcap)  // 50 Billion market cap
                .to(stringSerde, new JsonPOJOSerde<>(Stock.class), largecapTopic);

        streams = new KafkaStreams(builder, config);
        streams.cleanUp();  // Reset the application against the Kafka cluster before beginning.
        streams.start();
    }

    @PreDestroy
    public void closeStream() {
        streams.close();
    }
}
