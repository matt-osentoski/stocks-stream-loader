package com.garagebandhedgefund.stocks.batchjob.bindy;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * This bindy is used to Map CSV files from the custom stocks CSV file.  This file contains individual stocks that
 * should be created in the datasource, but are absent from the NASDAQ files.  For example, ETF/ETN symbols.
 */
@CsvRecord( separator = ",", skipFirstLine = true )
public class CustomStockBindy {

    @DataField(pos = 1, trim = true)
    private String symbol;

    @DataField(pos = 2, trim = true)
    private String name;

    @DataField(pos = 3, trim = true)
    private String marketCap;

    @DataField(pos = 4, trim = true)
    private String ipoYear;

    @DataField(pos = 5, trim = true)
    private String sector;

    @DataField(pos = 6, trim = true)
    private String industry;

    @DataField(pos = 7, trim = true)
    private String isIndex;

    @DataField(pos = 8, trim = true)
    private String isETF;

    @DataField(pos = 9, trim = true)
    private String isShortEtf;

    @DataField(pos = 10, trim = true)
    private String etfLeverageMultiplier;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public String getIpoYear() {
        return ipoYear;
    }

    public void setIpoYear(String ipoYear) {
        this.ipoYear = ipoYear;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getIsETF() {
        return isETF;
    }

    public void setIsETF(String isETF) {
        this.isETF = isETF;
    }

    public String getIsShortEtf() {
        return isShortEtf;
    }

    public void setIsShortEtf(String isShortEtf) {
        this.isShortEtf = isShortEtf;
    }

    public String getEtfLeverageMultiplier() {
        return etfLeverageMultiplier;
    }

    public void setEtfLeverageMultiplier(String etfLeverageMultiplier) {
        this.etfLeverageMultiplier = etfLeverageMultiplier;
    }

    public String getIsIndex() {
        return isIndex;
    }

    public void setIsIndex(String isIndex) {
        this.isIndex = isIndex;
    }
}
