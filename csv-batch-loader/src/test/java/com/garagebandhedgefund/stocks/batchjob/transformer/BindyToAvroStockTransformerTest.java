package com.garagebandhedgefund.stocks.batchjob.transformer;

import com.garagebandhedgefund.avro.model.Stock;
import com.garagebandhedgefund.stocks.batchjob.bindy.StockBindy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BindyToAvroStockTransformerTest {

    private StockBindy stockBindy;
    private BindyToAvroStockTransformer bindyToAvroStockTransformer;

    private final static String SYMBOL = "AAPL";
    private final static String NAME = "Apple Inc.";
    private final static String MARKET_CAP = "$887.95B";
    private final static long MARKET_CAP_LONG = 887950000000L;
    private final static String SECTOR = "Technology";
    private final static String INDUSTRY = "Computer Manufacturing";
    private final static String IPO_YEAR = "1980";

    @Before
    public void setup() {
        this.stockBindy = new StockBindy();
        stockBindy.setSymbol(SYMBOL);
        stockBindy.setName(NAME);
        stockBindy.setMarketCap(MARKET_CAP);
        stockBindy.setSector(SECTOR);
        stockBindy.setIndustry(INDUSTRY);
        stockBindy.setIpoYear(IPO_YEAR);
        bindyToAvroStockTransformer = new BindyToAvroStockTransformer();
    }

    @Test
    public void transform() {
        Stock transform = bindyToAvroStockTransformer.transform(stockBindy);
        assertNotNull(transform);
        assertEquals(SYMBOL, transform.getSymbol());
        assertEquals(NAME, transform.getName());
        assertEquals(new Long(MARKET_CAP_LONG), transform.getMarketCap());
        assertEquals(SECTOR, transform.getSector());
        assertEquals(INDUSTRY, transform.getIndustry());
        assertEquals(IPO_YEAR, transform.getIpoYear());
    }

}