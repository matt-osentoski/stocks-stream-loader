package com.garagebandhedgefund.stocks.batchjob.transformer;

import org.apache.avro.Schema;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.reflect.ReflectDatumWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @deprecated This class should no longer be used.  Use the Avro URL in the camel route instead.
 * Keeping this file for historical purposes
 *
 * @param <T>
 */
//@Component
public class PojoToAvroTransformer<T> {
    final static Logger logger = LoggerFactory.getLogger(PojoToAvroTransformer.class);

//    @Autowired
//    @Qualifier("stockSchema")
    Schema stockSchema;

//    @Autowired
//    @Qualifier("treasurySchema")
    Schema treasurySchema;

    public byte[] transformStock(T t) {
        return transform(t, stockSchema);
    }

    public byte[] transformTreasury(T t) {
        return transform(t, treasurySchema);
    }

    private byte[] transform(T t, Schema schema) {
        byte[] bytes = null;
        try {
            DatumWriter<Object> datumWriter = new ReflectDatumWriter<>(schema);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();


            BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(baos, null);
            //JsonEncoder encoder = EncoderFactory.get().jsonEncoder(schema, baos, false);
            datumWriter.write(t, encoder);
            encoder.flush();
            bytes = baos.toByteArray();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return bytes;
    }
}
