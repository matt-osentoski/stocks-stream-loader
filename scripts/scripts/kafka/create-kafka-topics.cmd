CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic stocks --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic treasuries-upsert --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic stock-fundamentals-upsert --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic stocks-largecap --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic stocks-custom --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic stock-prices-upsert --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic available-stocks-upsert --config retention.ms=3000000
timeout 2
CALL C:\development\tools\kafka-1.1.0-rc0\bin\windows\kafka-topics.bat --create --zookeeper 192.168.6.5:2181 --replication-factor 1  --partitions 1 --topic stock-price-intraday --config retention.ms=3000000
PAUSE
EXIT