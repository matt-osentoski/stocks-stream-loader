## Avro Schemas for Stock loading
This project creates java classes from Avro *.avsc files that will be used for the batch/stream
processing operations. 

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview  
**avro-schema** module  

## Build 
```mvn clean install```  
(NOTE: Running 'install' will run the 'avro' maven plugin to create the java classes under the
com.garagebandhedgefund.avro.model  package.)

## Creating a new Object
1. Create the *.avsc file under /src/main/avro  
2. Run the maven build command above (mvn clean install)
3. Commit and push the *.avsc and created *.java file into the git repo  
  
At this point your Jar file should be ready to use in other projects.
 