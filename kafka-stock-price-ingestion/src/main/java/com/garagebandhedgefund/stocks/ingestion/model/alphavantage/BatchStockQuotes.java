package com.garagebandhedgefund.stocks.ingestion.model.alphavantage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BatchStockQuotes {
    @JsonProperty("Stock Quotes")
    private List<StockQuote> stockQuotes = new ArrayList();

    public List<StockQuote> getStockQuotes() {
        return stockQuotes;
    }

    public void setStockQuotes(List<StockQuote> stockQuotes) {
        this.stockQuotes = stockQuotes;
    }
}
