## Kafka Stock Consumer
This module uses Spring Boot/Apache Camel to pull Stocks from Kafka and updates a database with the entries.  Other datasources can
be written to in the future.

## Repository
https://bitbucket.org/matt-osentoski/stocks-stream-loader/overview  
**kafka-stock-consumer** module

## Build (including docker images)
```mvn clean install```

## Kafka information
Stocks are pulled from the 'available-stocks-upsert' topic, then loaded into a database.