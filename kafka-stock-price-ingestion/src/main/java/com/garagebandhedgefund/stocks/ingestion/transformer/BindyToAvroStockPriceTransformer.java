package com.garagebandhedgefund.stocks.ingestion.transformer;

import com.garagebandhedgefund.avro.model.StockPrice;
import com.garagebandhedgefund.stocks.ingestion.bindy.StockPriceBindy;
import com.garagebandhedgefund.stocks.ingestion.model.iex.TradingStockPrice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class BindyToAvroStockPriceTransformer {
    final static Logger logger = LoggerFactory.getLogger(BindyToAvroStockPriceTransformer.class);

    public StockPrice transform(StockPriceBindy sourcePrice, String symbol) {
        StockPrice stockPrice = new StockPrice();
        stockPrice.setSymbol(symbol);
        stockPrice.setClosingPrice(sourcePrice.getClose());
        stockPrice.setHighPrice(sourcePrice.getHigh());
        stockPrice.setLowPrice(sourcePrice.getLow());
        stockPrice.setOpeningPrice(sourcePrice.getOpen());
        stockPrice.setPriceDate(sourcePrice.getTimestamp().getTime());
        stockPrice.setVolume(sourcePrice.getVolume());
        stockPrice.setPeriod("d");
        return stockPrice;
    }
}
