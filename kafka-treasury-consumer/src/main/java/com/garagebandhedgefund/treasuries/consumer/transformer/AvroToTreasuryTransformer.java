package com.garagebandhedgefund.treasuries.consumer.transformer;

import com.garagebandhedgefund.avro.model.Treasury;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class AvroToTreasuryTransformer {
    final static Logger logger = LoggerFactory.getLogger(AvroToTreasuryTransformer.class);

    public com.garagebandhedgefund.trading.model.Treasury transform(Treasury avroTreasury) {
        com.garagebandhedgefund.trading.model.Treasury treasury = new com.garagebandhedgefund.trading.model.Treasury();
        try {
            BeanUtils.copyProperties(treasury, avroTreasury);
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(), e);
        }
        return treasury;
    }
}
